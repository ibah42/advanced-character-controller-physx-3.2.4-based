Advanced Character Controller library with extra opportunities
based on PhysX 3.2.4 Kinematic Character Controller.

ACC v1.0 + Demo

Build on VS 2012

Used PhysX 3.2.4, OpenGL, Glut

Used environment variable - $(PHYSX_DIR) - path to PhysX 3.2.4 root directory

ACC compiled as a 32-bit static library.

You should copy files into "../ACC/Debug" and into "..ACC/Release"
directories for compilation and run

1) glut/glew/gl  		libs (*.lib)

2) glut/glew			Dlls (*.dll)

3) for "../ACC/Debug" 	use  PhysX 3.2.4 libraries
	"PhysX3CHECKED_x86.dll"
	"PhysX3CharacterKinematicCHECKED_x86.dll"
	"PhysX3CommonCHECKED_x86.dll"
	"PhysX3CookingCHECKED_x86.dll"	
	"PhysX3GpuCHECKED_x86.dll"

4) for "../ACC/Release" use  PhysX 3.2.4 libraries
	"PhysX3_x86.dll"
	"PhysX3CharacterKinematic_x86.dll"
	"PhysX3Common_x86.dll"
	"PhysX3Cooking_x86.dll"	
	"PhysX3Gpu_x86.dll"

You can use extracted files from next archives
https://www.dropbox.com/s/dv0mpfynu2cwg7u/ACC_debug.zip		(for debug 32) to "../ACC/Debug"
https://www.dropbox.com/s/5nen3268amny656/ACC_release.zip	(for release 32) to "..ACC/Release"

contact support: ibah.acc@gmail.com