/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/


#ifndef __ACC_AUTO_CONFIG_HPP__
#define __ACC_AUTO_CONFIG_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include <PxPhysicsAPI.h>
#include <characterkinematic/PxController.h>
#include <extensions/PxDefaultSimulationFilterShader.h>
#include <foundation/PxMat33.h>

#include <windows.h>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

typedef const physx::PxVec3& InVec3;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#if defined( _DEBUG ) || defined( DEBUG )
	#define ACC_DEBUG
	#define _ACC_DEBUG
#endif


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


//if uncommented - concurrent simulation of CC's enabled ( OMP )
//#define ACC_USE_CONCURRENT_CC_SIMULATION


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#if	( defined( WIN32 ) || defined( WIN32 ) ) && !( defined( WIN64 ) || defined ( _WIN64 ) )
	#if defined ACC_DEBUG
		#pragma comment ( lib, "PxTaskCHECKED.lib" )
		#pragma comment ( lib, "PhysX3CommonCHECKED_x86.lib" )
		#pragma comment ( lib, "PhysX3CHECKED_x86.lib" )
		#pragma comment ( lib, "PhysX3CookingCHECKED_x86.lib" )	
		#pragma comment ( lib, "PhysX3CharacterKinematicCHECKED_x86.lib" )
		#pragma comment ( lib, "PhysX3ExtensionsCHECKED.lib" )
		#pragma comment ( lib, "PhysX3VehicleCHECKED.lib" )
		#pragma comment ( lib, "RepX3CHECKED.lib" )
		#pragma comment ( lib, "RepXUpgrader3CHECKED.lib" )
		#pragma comment ( lib, "PhysXProfileSDKCHECKED.lib" )	
		#pragma comment ( lib, "PhysXVisualDebuggerSDKCHECKED.lib" )
	#else
		#pragma comment ( lib, "PxTask.lib" )
		#pragma comment ( lib, "PhysX3Common_x86.lib" )
		#pragma comment ( lib, "PhysX3_x86.lib" )
		#pragma comment ( lib, "PhysX3Cooking_x86.lib" )	
		#pragma comment ( lib, "PhysX3CharacterKinematic_x86.lib" )
		#pragma comment ( lib, "PhysX3Extensions.lib" )
		#pragma comment ( lib, "PhysX3Vehicle.lib" )
		#pragma comment ( lib, "RepX3.lib" )
		#pragma comment ( lib, "RepXUpgrader3.lib" )
		#pragma comment ( lib, "PhysXProfileSDK.lib" )	
		#pragma comment ( lib, "PhysXVisualDebuggerSDK.lib" )
	#endif

#else if defined( WIN64 ) || defined ( _WIN64 )
	#if defined ACC_DEBUG
		#pragma comment ( lib, "PxTaskCHECKED.lib" )
		#pragma comment ( lib, "PhysX3CommonCHECKED_x64.lib" )
		#pragma comment ( lib, "PhysX3CHECKED_x64.lib" )
		#pragma comment ( lib, "PhysX3CookingCHECKED_x64.lib" )	
		#pragma comment ( lib, "PhysX3CharacterKinematicCHECKED_x64.lib" )
		#pragma comment ( lib, "PhysX3ExtensionsCHECKED.lib" )
		#pragma comment ( lib, "PhysX3VehicleCHECKED.lib" )
		#pragma comment ( lib, "RepX3CHECKED.lib" )	
		#pragma comment ( lib, "RepXUpgrader3CHECKED.lib" )
		#pragma comment ( lib, "PhysXProfileSDKCHECKED.lib" )	
		#pragma comment ( lib, "PhysXVisualDebuggerSDKCHECKED.lib" )
	#else
		#pragma comment ( lib, "PxTask.lib" )
		#pragma comment ( lib, "PhysX3Common_x64.lib" )
		#pragma comment ( lib, "PhysX3_x64.lib" )
		#pragma comment ( lib, "PhysX3Cooking_x64.lib" )	
		#pragma comment ( lib, "PhysX3CharacterKinematic_x64.lib" )
		#pragma comment ( lib, "PhysX3Extensions.lib" )
		#pragma comment ( lib, "PhysX3Vehicle.lib" )
		#pragma comment ( lib, "RepX3.lib" )	
		#pragma comment ( lib, "RepXUpgrader3.lib" )
		#pragma comment ( lib, "PhysXProfileSDK.lib" )	
		#pragma comment ( lib, "PhysXVisualDebuggerSDK.lib" )
	#endif
#endif


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ACC_AUTO_CONFIG_HPP__