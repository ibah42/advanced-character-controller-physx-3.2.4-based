/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/


#ifndef __ACC_PCH_HPP__
#define __ACC_PCH_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include <limits.h>
#include <float.h>
#include <stdlib.h>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <list>
#include <hash_set>
#include <algorithm>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "ACCSources/AutoConfiguration.hpp"

#include "ACCSources/Common/Basement/Optional.hpp"
#include "ACCSources/Common/Basement/RangeAcceleratingValue.hpp"
#include "ACCSources/Common/Basement/InertValue.hpp"
#include "ACCSources/Common/Basement/Switch.hpp"
#include "ACCSources/Common/Basement/Utility.hpp"
#include "ACCSources/Common/Basement/CosineInterpolationCurve.hpp"
#include "ACCSources/Common/Basement/Angle.hpp"
#include "ACCSources/Common/Basement/Rotation.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ACC_PCH_HPP__
