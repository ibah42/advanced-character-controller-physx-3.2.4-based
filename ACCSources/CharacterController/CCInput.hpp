/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __ACC_CharacterControllerInput_HPP__
#define __ACC_CharacterControllerInput_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "ACCSources/CharacterController/AbstractCC.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class CCInput
	:	public AbstractCC
{

public:

	CCInput()							{ resetInput();					}

	virtual void strafeLeftOn()			{ mMoveLeftActive = true;		}
	virtual void strafeLeftOff()		{ mMoveLeftActive = false;		}

	virtual void strafeRightOn()		{ mMoveRightActive = true;		}
	virtual void strafeRightOff()		{ mMoveRightActive = false;		}

	virtual void moveForwardOn()		{ mMoveForwardActive = true;	}
	virtual void moveForwardOff()		{ mMoveForwardActive = false;	}

	virtual void moveBackwardOn()		{ mMoveBackwardActive = true;	}
	virtual void moveBackwardOff()		{ mMoveBackwardActive = false;	}

	virtual void jumpOn()				{ mUserContinueJumping = true; onActivateJump();	}
	virtual void jumpOff()				{ mUserContinueJumping = false;						}

	virtual void duckDown()				{ mIsDuckingDown = true;		}
	virtual void duckUp()				{ mIsDuckingDown = false;		}

	virtual void run()					{ mIsRunning = true;			}
	virtual void walk()					{ mIsRunning = false;			}

	virtual void resetInput()
	{
		mMoveForwardActive = false;
		mMoveBackwardActive = false;
		mMoveLeftActive = false;
		mMoveRightActive = false;
		mIsDuckingDown = false;
		mIsRunning = false;
		mUserContinueJumping = false;
	}


protected:
	
	virtual void onActivateJump() = 0;

	bool isUserDucking() const				{ return mIsDuckingDown;		}
	bool isUserRunning() const				{ return mIsRunning;			}
	bool isUserMovingForward() const		{ return mMoveForwardActive;	}
	bool isUserMovingBackward() const		{ return mMoveBackwardActive;	}
	bool isUserMovingLeft() const			{ return mMoveLeftActive;		}
	bool isUserMovingRight() const			{ return mMoveRightActive;		}
	bool isUserJumping() const				{ return mUserContinueJumping;	}

private:
	
	bool mIsRunning;
	bool mIsDuckingDown;
	bool mUserContinueJumping;

	bool mMoveForwardActive;
	bool mMoveBackwardActive;
	bool mMoveLeftActive;
	bool mMoveRightActive;
			
}; // class CharacterControllerInput


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ACC_CharacterControllerInput_HPP__
