/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __ACC_LOCABLE_CONTROLLER_HPP__
#define __ACC_LOCABLE_CONTROLLER_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

class LockablePxController;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct OverLockException
	:	public std::exception
{
	OverLockException( const char* _str ) :	std::exception( _str ) {}
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class CCAutoLock
{
public:
	
	CCAutoLock( LockablePxController * _locker );
	
	CCAutoLock( CCAutoLock& other );
	
	CCAutoLock& operator = ( CCAutoLock & other );
	
	~CCAutoLock();
	
	void die();
	
	
private:

	LockablePxController* mLockIFace;

}; // class CCAutoLock


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class  LockablePxController
{

public:

	inline LockablePxController( PxCapsuleController* controller )
		:	mController( controller )
		,	mIsLocked( 0 )
	{}

	void set( PxCapsuleController* controller )
	{
		if( mController )
			unlock();

		mController = controller;
	}

	inline PxCapsuleController* operator -> ()
	{
		return mController;
	}

	inline const PxCapsuleController* operator -> () const
	{
		return mController;
	}

	inline PxCapsuleController* get()
	{
		return mController;
	}

	inline const PxCapsuleController* get() const
	{
		return mController;
	}
	
	inline PxControllerShapeType::Enum getType() const
	{
		return mController->getType();
	}

	inline void move( const PxVec3& disp, PxU32 activeGroups, float timeStep, PxU32& collisionFlags, PxObstacleContext* context = 0 )
	{
		PxControllerFilters filter;
		filter.mActiveGroups = activeGroups;
		filter.mFilterFlags |= PxSceneQueryFilterFlag::eMESH_MULTIPLE;
		filter.mFilterFlags &= ~( PxSceneQueryFilterFlag::ePREFILTER | PxSceneQueryFilterFlag::ePOSTFILTER );

		lock();
		collisionFlags = mController->move( disp, 0, timeStep, filter, context );
		unlock();

		checkNumericalOverflowV3( mController->getPosition() );
	}
	
	inline void setPosition( const PxVec3& position )
	{
		lock();
		mController->setPosition( toVector3<PxExtendedVec3>( position ) );
		checkNumericalOverflowV3( mController->getPosition() );
		unlock();
	}
	
	inline void setPositionExt( const PxExtendedVec3& position )
	{
		lock();
		mController->setPosition( position );
		checkNumericalOverflowV3( mController->getPosition() );
		unlock();
	}

	inline float getSkinOffset() const
	{
		return mController->getContactOffset();
	}

	inline PxVec3 getPosition() const
	{
		checkNumericalOverflowV3( mController->getPosition() );
		return toVector3<PxVec3>( mController->getPosition() );
	}

	inline PxExtendedVec3 getPositionExt() const
	{
		checkNumericalOverflowV3( mController->getPosition() );
		return mController->getPosition();
	}

	inline PxActor* getActor() const
	{
		return mController->getActor();
	}

	inline void setStepOffset( const float offset )
	{
		lock();
		mController->setStepOffset( offset );
		unlock();
	}

	inline PxCCTInteractionMode::Enum getInteraction() const
	{
		return mController->getInteraction();
	}

	inline void setInteraction( PxCCTInteractionMode::Enum mode )
	{
		lock();
		mController->setInteraction( mode );
		unlock();
	}

	inline void invalidateCache()
	{
		lock();
		mController->invalidateCache();
		checkNumericalOverflowV3( mController->getPosition() );
		unlock();
	}

	inline void* getUserData() const
	{
		return mController->getUserData();
	}

	inline void resize( float height )
	{
		lock();
		mController->resize( height );
		unlock();
	}

	inline void releaseController()
	{
		if( !mController )
			return;

		lock();
		mController->release();
		mController = 0;
		unlock();        
	}

	void lock();
	void unlock();

	inline CCAutoLock autoLock()
	{
		return CCAutoLock( this );
	}

	inline ~LockablePxController()
	{
		releaseController();
	}

	//dangerous!
	void forceUnLock();

private:

	int mIsLocked;
	PxCapsuleController* mController;

}; // class  LockablePxController


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#endif // __ACC_LOCABLE_CONTROLLER_HPP__