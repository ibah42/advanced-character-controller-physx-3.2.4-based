/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __ACC_CCManager_HPP__
#define __ACC_CCManager_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "ACCSources/CharacterController/AbstractCC.hpp"
#include "ACCSources/CharacterController/CCProperties.hpp"
#include "ACCSources/CharacterController/CCBroadcaster.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{
		
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class CCManager
	:	public NonCopyable
{
public:

	typedef std::set< AbstractCC * > CCSet;

	typedef std::map< const PxShape*, AbstractCC* > CCByShapes;
	typedef std::map< const PxActor*, AbstractCC* > CCByActors;

	typedef std::pair< const PxShape*, AbstractCC* > CCAndShapePair;
	typedef std::pair< const PxActor*, AbstractCC* > CCAndActorPair;

	CCManager(
			PxPhysics& physics
		,	PxScene& scene
		,	PxControllerManager& physXControllerManager
		,	PxU32 charactercontrollersCollisionGroup
	);

	AbstractCC& buildCC( const CCDescription & description );

	void updateAllControllers( float timeStep );
	void removeController( AbstractCC* that );


	DefaultCCFrictionAdapter& getDefaultCharacterControllerFrictionAdapter()				{ return mDefaultCharacterFrictionAdapter; }
	const DefaultCCFrictionAdapter& getDefaultCharacterControllerFrictionAdapter() const	{ return mDefaultCharacterFrictionAdapter; }


	CCSet& getCharacters()										{ return mAllCharacters; }
	AbstractCCInput& getBroadcaster()							{ return mCCBroadcaster; }

	PxControllerManager& getPhysXControllerManager()			{ return mPxControllerManager; }
	const PxControllerManager& getPhysXControllerManager() const{ return mPxControllerManager; }

	PxScene& getPhysxScene()									{ return mScene;							}
	const PxScene& getPhysxScene()const							{ return mScene;							}	

	PxPhysics& getPhysics()										{ return mPhysics;							}
	const PxPhysics& getPhysics() const							{ return mPhysics;							}

	void setCharacterControllerPxMaterial( PxMaterial* mat )	{ mCharacterControllerPxMaterial = mat;		}
	const PxMaterial* getCharacterControllerPxMaterial() const	{ return mCharacterControllerPxMaterial;	}
	PxMaterial* getCharacterControllerPxMaterial()				{ return mCharacterControllerPxMaterial;	}

	PxU32 getCharacterControllerCollisionGroup() const			{ return mCCCollisionGroup; }

	bool isCharactersShape( const PxShape* shape ) const;

	// would return 0 if wouldn't find
	AbstractCC* tryCastShape( const PxShape* shape ) const;
	AbstractCC* tryCastActor( const PxActor* actor ) const;

private:

	PxShape* getCCShape( AbstractCC& cc ) const;
	
private:
	
	CCBroadcaster mCCBroadcaster;

	DefaultCCFrictionAdapter mDefaultCharacterFrictionAdapter;
	PxControllerManager& mPxControllerManager;
	PxScene& mScene;
	PxPhysics& mPhysics;
	PxU32 mCCCollisionGroup;
	
	CCByActors mCharactersActors;
	CCByShapes mCharactersShapes;
	CCSet mAllCharacters;

	PxMaterial* mCharacterControllerPxMaterial;
	
}; // class CCManager


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ACC_CCManager_HPP__
