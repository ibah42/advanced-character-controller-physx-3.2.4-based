/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __ACC_CharacterControllerCore_HPP__
#define __ACC_CharacterControllerCore_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "ACCSources/CharacterController/CCGameParameters.hpp"
#include "ACCSources/CharacterController/AbstractCC.hpp"
#include "ACCSources/CharacterController/CCBody.hpp"
#include "ACCSources/Common/Basement/TimeSimulators.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	// TODO: mustn't be too small, cause it can produce penetration-volume, unstable simulation
	const float CC_OFFSET = 0.001f;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct JumpFrameStage
{
	JumpFrameStage()
		:	mJumpInitialVerticalVelocity( 0 )
	{
		reset();
	}

	void reset()
	{
		mIsDucking = false;
		mDuckingElapsedTime = 0;
	}

	bool isDucking() const	{ return mIsDucking; }

	void activateClassicJumpWithDucking()
	{
		mJumpInitialVerticalVelocity = 0;
		mIsDucking = true;
	}

	bool canMakeNewJump() const { return !mIsDucking; }

public:

	float mJumpInitialVerticalVelocity;
	bool mIsDucking;
	float mDuckingElapsedTime;

}; // struct JumpFrameStage


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class CCCore
	:	public CCGameParameters
	,	public CCBodyListener
{

public:

	CCCore( const CCDescription & description, CCManager & CCManager );

	virtual void onCalculateNextAppliedForce( float realPrevElevationAngle );
		
	virtual void update( float timeStep );
	
	void setCharacterPos( PxVec3 pos );
	virtual RefOptional< PxVec3 > getBestFootingFaceDir() const;
	virtual ValueOptional< Angle > getFootingOrientationAngle() const;
	virtual RefOptional< PxVec3 > getContactNormal() const			{ return mCCBody.getFootingContactNormal();	}
	virtual RefOptional< ImpactVector > getFootingImpactData() const{ return mCCBody.getFootingImactData();		}
		
	PxActor* getCharacterActor()									{ return getPhysxController().getActor();	}
	virtual PxRigidDynamic& getCharacterRigidDynamic();

	virtual LockablePxController& getPhysxController()				{ return mCCBody.getPhysxController();		}
	virtual const LockablePxController& getPhysxController() const	{ return mCCBody.getPhysxController();		}
	virtual PxVec3 getCurrentVectorVelocity() const					{ return mCCBody.getCurrentVectorVelocity(); }

	PxU32 getCurrentCollisions() const								{ return mCCBody.getCurrentCollisionFlags(); }
	
	virtual PxVec3 getRealPosition() const							{ return mCCBody.getRealPosition();			}
	virtual PxVec3 getInterpolatedPosition() const					{ return mCCBody.getInterpolatedPosition();	}

	~CCCore()														{ /* do not delete used PxController*  ! */ }
	virtual float getCurrentHeight() const							{ return mCurrentHeight;					}
	virtual const CCManager& getManager() const						{ return mCCManager;		}
	virtual CCManager& getManager()									{ return mCCManager;		}
	
	virtual void setDirection( InVec3 dir );
	virtual PxVec3 getDirection() const;
	virtual Angle getDirectionAngle() const							{ return mCurrentFaceAngle; }

protected:

	virtual void onActivateJump();
	
private:

	inline void assertHeight(){ Assert( mCurrentHeight<= getControllerMaxHeight() && mCurrentHeight >= getControllerMinHeight() ); }

	bool checkVolume( float radius, float pxHeight, InVec3 pos ) const;
	virtual bool checkVolume() const;

	void resizeHeightOnGround( const float delta );
	void resizeHeightInFlight( const float delta );

	bool isCoreOrderingDucking();
	
	void applyForwardBackwardAcceleration(
			PxVec3& inOutValue
		,	bool isNegative
		,	InVec3 charCtrlDirection
	);
	void applyLeftRightAcceleration(
			PxVec3& inOutValue
		,	bool isNegative
		,	InVec3 charCtrlDirection
	);
	void prepareAfterJumpScenarios();

	void updateCharacterExtents();
	void updateJumping();
	void updateMovementProps( float angleDegrees );

protected:

	CCManager & mCCManager; // Singleton
	CCBody mCCBody;

private:

	Angle mCurrentFaceAngle;
	
	bool mJumpLaunched;
	JumpFrameStage mJumpStage;

	float mAccumulatedTimeSinceLastJumpFinished;
	float mDt;
	float mCurrentHeight;
	float mCurrentJumpAcceleration;
	
	std::auto_ptr< TimeSimulator > mTimeSimulator;
	CCMovementProperties* mSelectedMovementProps;

	float mIdealAcceleration;
	float mIdealMaxVelocity;

	PxControllerShapeType::Enum mBodyType;
		
}; // class CharacterControllerCore


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ACC_CharacterControllerCore_HPP__
