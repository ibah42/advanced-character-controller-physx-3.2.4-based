/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#include "ACCSources/PCH/ACCPCH.hpp"
#include "ACCSources/CharacterController/LockablePxController.hpp"
#include "ACCSources/CharacterController/CCManager.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
LockablePxController::lock()
{
#ifdef ACC_USE_CONCURRENT_CC_SIMULATION

	if ( mIsLocked == 0 )
	{
		//m_controller->getActor()->getScene()->lockRead( __FILE__, __LINE__ );
		mController->getActor()->getScene()->lockWrite( __FILE__, __LINE__ );
	}
		
	++ mIsLocked;

	if ( mIsLocked > 10*100 )
		throw OverLockException( "LockablePxController::lock > 10000" );

	Assert( mIsLocked >= 0 && mIsLocked< 10*100 );

#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
LockablePxController::unlock()
{
#ifdef ACC_USE_CONCURRENT_CC_SIMULATION

	if( mIsLocked == 1 )
	{
		mIsLocked = 0;
		//m_controller->getActor()->getScene()->unlockRead();
		mController->getActor()->getScene()->unlockWrite();
	}
	else
		--mIsLocked;

	Assert( mIsLocked >= 0 );

#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
LockablePxController::forceUnLock()
{
#ifdef ACC_USE_CONCURRENT_CC_SIMULATION

	if ( mIsLocked > 0 )
	{
		mIsLocked = 0;
		unlock();
	}

#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#ifdef ACC_USE_CONCURRENT_CC_SIMULATION

CCAutoLock::CCAutoLock( LockablePxController * _locker )
	:	mLockIFace( _locker )
{
	mLockIFace->lock();
}

#else

CCAutoLock::CCAutoLock( LockablePxController * _locker )
{}

#endif



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CCAutoLock::CCAutoLock( CCAutoLock& other )
{
#ifdef ACC_USE_CONCURRENT_CC_SIMULATION

	mLockIFace = other.mLockIFace;
	other.mLockIFace = 0;

#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CCAutoLock&
CCAutoLock::operator = ( CCAutoLock & other )
{
#ifdef ACC_USE_CONCURRENT_CC_SIMULATION

	if ( mLockIFace )
		mLockIFace->unlock();

	mLockIFace = other.mLockIFace;
	other.mLockIFace = 0;

#endif

	return *this;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


CCAutoLock::~CCAutoLock()
{
#ifdef ACC_USE_CONCURRENT_CC_SIMULATION

	die();

#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCAutoLock::die()
{
#ifdef ACC_USE_CONCURRENT_CC_SIMULATION

	if ( mLockIFace )
		mLockIFace->unlock();

	mLockIFace = 0;

#endif
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
