/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#include "ACCSources/PCH/ACCPCH.hpp"
#include "ACCSources/CharacterController/CCManager.hpp"
#include "ACCSources/CharacterController/CCCore.hpp"
#include <omp.h>

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	
CCManager::CCManager(
		PxPhysics& physics	
	,	PxScene& scene
	,	PxControllerManager& physXControllerManager
	,	PxU32 charactercontrollersCollisionGroup
)
	:	mScene( scene )
	,	mPhysics( physics )
	,	mPxControllerManager( physXControllerManager )
	,	mCCCollisionGroup( charactercontrollersCollisionGroup )
	,	mDefaultCharacterFrictionAdapter( &scene )
	,	mCharacterControllerPxMaterial( 0 )
	,	mCCBroadcaster( mAllCharacters )
{
	mCharacterControllerPxMaterial = mPhysics.createMaterial( 0.3f, 0.3f, 0.1f );
	
#ifdef ACC_DEBUG

	Assert( 0 / returnCorrectDivider( 0 ) < returnCorrectDivider( 0 ) );
	PxVec3 a( 1, -100500,  1 );
	PxVec3 b( 0, -100500, -1 );

	PxVec3 c( -1, -100500,  1 );
	PxVec3 d( 1, -100500, -1 );

	float d1 =  getXZAngleBetweenVectors( a,b );
	float d2 =  getXZAngleBetweenVectors( a,-b );
	float d3 =  getXZAngleBetweenVectors( c,d );
	float d4 =  getXZAngleBetweenVectors( d,c );
	float d5 =  getXZAngleBetweenVectors( d,d );
	float d6 =  getXZAngleBetweenVectors( a,c );
	float d7 =  getXZAngleBetweenVectors( c,a );

	float dot1 = cosAngleXZ( a, b );
	float dot2 = cosAngleXZ( c, d );

#define DOT_ASSERT( angleIdle, dot ) \
	Assert( fabs( angleIdle - Angle::acos( dot ).getDegrees() ) < 0.01 );

#define ANGLE_ASSERT( angle, value ) \
	Assert( angle > value - 0.01 && angle < value + 0.01 )
	
	DOT_ASSERT( 135, dot1 );
	DOT_ASSERT( 180, dot2 );

	ANGLE_ASSERT( d1 , 135 );
	ANGLE_ASSERT( d2 , -45 );
	ANGLE_ASSERT( d3 , 180 );
	ANGLE_ASSERT( d4 , 180 );
	ANGLE_ASSERT( d5 , 0 );
	ANGLE_ASSERT( d6 , -90 );
	ANGLE_ASSERT( d7 , 90 );

#undef DOT_ASSERT
#undef ANGLE_ASSERT

#endif


	mPxControllerManager.setTessellation( false, 10000.f );

} // CCManager::CCManager


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCManager::updateAllControllers( float timeStep )
{
	// correct penetration effects with other CC's
	mPxControllerManager.computeInteractions( timeStep );

	CCSet::iterator begin = mAllCharacters.begin();
	CCSet::iterator end = mAllCharacters.end();

	std::vector< AbstractCC* > tempVector;
	tempVector.reserve( mAllCharacters.size() + 1 );

	while( begin != end )
	{
		tempVector.push_back( *begin );
		++begin;
	}

	//using OMP and possibly other concurrent simulation SW might not be 
	//stable for PhysX, causes concurrent write while reading operations in progress ( by concurrent threads )
	//__intrinsic logic might be correct, but it's not safe.
	//plus efficiency of concurrent CC simulation is not quiet high
	//so it would be better to do nothing concurrent( read or write to PhysX SDK )
	//while CC's are sequentially simulated

	int size = tempVector.size();

	if( size > 32 )
	{
#ifdef ACC_USE_CONCURRENT_CC_SIMULATION
		omp_set_num_threads( 2 );
		#pragma omp parallel for schedule( dynamic, 8 )
#endif
		for ( int i = 0; i < size; i++ )
			tempVector[ i ]->update( timeStep );
	}
	else
		for ( int i = 0; i < size; i++ )
			tempVector[ i ]->update( timeStep );

} // CCManager::updateAllControllers


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
CCManager::removeController( AbstractCC* that )
{
	Assert( that );

	CCSet::iterator found = mAllCharacters.find( that );
	if ( found == mAllCharacters.end() )
		throw std::exception( "ACCSources/CCManager::removeController arg wasn't found" );

	CCCore* asCharacterController = 0;
	asCharacterController = static_cast< CCCore* >( that );
	
	mAllCharacters.erase( that );
	LockablePxController & restedCtrl = asCharacterController->getPhysxController();
	restedCtrl.forceUnLock();

	mCharactersShapes.erase( getCCShape( *asCharacterController ) );
	mCharactersActors.erase( asCharacterController->getCharacterActor() );

	delete asCharacterController; // here is deleted  PxController!

} // CCManager::removeController


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


AbstractCC&
CCManager::buildCC( const CCDescription & description )
{
	CCCore* newCharCtrl = new CCCore( description, *this );

	mAllCharacters.insert( newCharCtrl );

	mCharactersActors.insert( CCAndActorPair( newCharCtrl->getCharacterActor(), newCharCtrl ) );
	mCharactersShapes.insert( CCAndShapePair( getCCShape( *newCharCtrl ), newCharCtrl ) );
	return *newCharCtrl;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


PxShape*
CCManager::getCCShape( AbstractCC& cc ) const
{
	PxShape* shape = 0;
	cc.getCharacterRigidDynamic().isRigidDynamic()->getShapes( &shape, 1, 0 );
	Assert( shape );
	return shape;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


AbstractCC*
CCManager::tryCastShape( const PxShape* shape ) const
{
	CCByShapes::const_iterator found = mCharactersShapes.find( shape );
	if ( found == mCharactersShapes.end() )
		return 0;
	else
		return ( *found ).second;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


AbstractCC*
CCManager::tryCastActor( const PxActor* actor ) const
{
	CCByActors::const_iterator found = mCharactersActors.find( actor );
	if ( found == mCharactersActors.end() )
		return 0;
	else
		return ( *found ).second;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/