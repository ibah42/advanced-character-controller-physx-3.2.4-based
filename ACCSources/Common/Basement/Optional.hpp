/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __ACC_OPTIONAL_HPP__
#define __ACC_OPTIONAL_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace OptionalImpl{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
class BaseSimpleOptional
{
public:

	BaseSimpleOptional()										{ mInit = false; }
	BaseSimpleOptional( const BaseSimpleOptional< T >& that )	{ reset(that); }
	void operator = ( const BaseSimpleOptional< T >& that )		{ reset(that); }

	operator bool() const		{ return mInit; }
	bool isInit() const			{ return mInit; }
		
	void reset( const BaseSimpleOptional< T >& that )
	{
		if( that.isInit() )
		{
			mInit = true;
			memcpy( &mRawData, &(that.mRawData), sizeof(T) );
		}
		else
			mInit = false;
	}
	
protected:

	template< typename T >
	struct Holder
	{
		char data[ sizeof(T) ];
	};

	bool mInit;
	Holder<T> mRawData;

}; // class BaseSimpleOptional


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace OptionalImpl

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
class RefOptional
	:	public OptionalImpl::BaseSimpleOptional< T >
{
public:

	void reset() { mInit = false; }

	void reset( const T data )
	{
		mInit = true;
		memcpy( &mRawData, &data, sizeof(T) );
	}
	
	RefOptional(){}

	RefOptional( const T data )			{ reset( data );	}
	void operator = ( const T & data )	{ reset( data );	}

	T& operator * ()					{ return get();		}
	T* operator -> ()					{ return &get();	}

	const T& operator * () const		{ return get();		}
	const T* operator -> ()	const		{ return &get();	}
	
	T& get()
	{
		Assert( mInit );
		return reinterpret_cast< T& >( mRawData );
	}
	
	const T& get() const
	{
		Assert( mInit );
		return reinterpret_cast< const T& >( mRawData );
	}

}; // class RefOptional


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


template< typename T >
class ValueOptional
	:	public OptionalImpl::BaseSimpleOptional< T >
{
public:

	void reset() { mInit = false; }
	
	void reset( const T& data )
	{
		mInit = true;
		memcpy( &mRawData, &data, sizeof(T) );
	}

	ValueOptional(){}

	ValueOptional( const T data )		{ reset( data );	}
	void operator = ( const T data )	{ reset( data );	}

	T operator * ()	const				{ return get();		}
	T operator -> () const				{ return get();		}
	T get()	const
	{
		Assert( mInit );
		return *reinterpret_cast< const T* >( &mRawData );
	}

}; // class ValueOptional


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ACC_OPTIONAL_HPP__