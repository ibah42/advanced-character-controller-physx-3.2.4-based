/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __ACC_ANGLE_HPP__
#define __ACC_ANGLE_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#pragma warning( disable : 4305 4244 )

	const float Pi		=	3.14159265359;
	const float TwoPi	=	Pi * 2.0;
	const float G		=	9.80665;

#pragma warning( default : 4305 4244 )


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Angle
{

private:

	Angle( float radians )
		:	mRadians( radians )
	{}

public:

	static Angle buildRadians( float radians )
	{
		return Angle( radians );
	}
	
	static Angle buildDegrees( float degrees )
	{
		Angle a;
		a.setDegrees( degrees );
		return a;
	}

	inline Angle()
		:	mRadians(0)
	{}

	inline void div( float d )
	{
		mRadians /= d;
	}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	inline bool operator < ( const Angle a )
	{
		return mRadians < a.mRadians;
	}

	inline bool operator > ( const Angle a )
	{
		return mRadians > a.mRadians;
	}

	inline bool operator == ( const Angle a )
	{
		return mRadians == a.mRadians;
	}

	inline bool operator != ( const Angle a )
	{
		return mRadians != a.mRadians;
	}

	inline Angle operator + ( const Angle a )
	{
		return Angle( mRadians + a.mRadians );
	}

	inline Angle operator - ( const Angle a )
	{
		return Angle( mRadians - a.mRadians );
	}

	inline Angle operator - ()
	{
		return Angle( - mRadians );
	}

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

	inline float getRadians() const
	{
		return mRadians;
	}

	inline float getDegrees() const
	{
		return mRadians / Pi * 180;
	}

	inline void setRadians( float radians )
	{
		mRadians = radians;
	}

	inline void setDegrees( float degrees )
	{
		mRadians = degrees * Pi / 180;
	}

	inline float cos() const
	{
		return ::cos( mRadians );
	}
	
	inline float sin() const
	{
		return ::sin( mRadians );
	}
	
	inline float tan() const
	{
		return ::tan( mRadians );
	}
	
	static Angle atan( float trygValue )
	{
		return Angle ( ::atan( trygValue ) );
	}

	static Angle acos( float trygValue )
	{
		if ( trygValue >= 1 )
			return Angle ( ::acos( 1.f ) );

		if ( trygValue <= -1 )
			return Angle ( ::acos( -1.f ) );

		return Angle ( ::acos( trygValue ) );
	}

	static Angle asin( float trygValue )
	{
		if ( trygValue >= 1 )
			return Angle ( ::asin( 1.f ) );

		if ( trygValue <= -1 )
			return Angle ( ::asin( -1.f ) );

		return Angle ( ::asin( trygValue ) );
	}


	template< typename TVec3 >
 	static Angle angleBetweenVectors3( const TVec3& vec1, const TVec3& vec2 )
 	{		
 		float lenProduct = magnitudeXZ( vec1 ) * magnitudeXZ( vec2 );
 		lenProduct = returnCorrectDivider( lenProduct );
 
 		float f = ( vec1.x * vec2.x  +  vec1.z * vec2.z ) / lenProduct;
  		clamp( -1.0f, 1.0f, f );
 		return Angle::acos( f );
 	}

private:

	float mRadians;

}; // class Angle


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#endif // __ACC_ANGLE_HPP__
