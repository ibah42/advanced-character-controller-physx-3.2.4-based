/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __ACC_ROTATION_HPP__
#define __ACC_ROTATION_HPP__

#include "ACCSources/Common/Basement/Angle.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Rotation
{
public:

	inline Rotation()
		:	mRotationsPerSec(0)
	{}

	friend inline Rotation operator * ( float ratio, const Rotation& rotation );
	friend inline Rotation operator * ( const Rotation& rotation, float ratio );

	inline float getRPM() const;
	inline float getRPS() const;
	inline float getRadiansPS() const;
	inline float getRadiansPM() const;
	inline float getDegreesPS() const;
	inline float getDegreesPM() const;

	inline void setRPM( float value );
	inline void setRPS( float value );
	inline void setRadiansPS( float value );
	inline void setRadiansPM( float value );
	inline void setDegreesPS( float value );
	inline void setDegreesPM( float value );

private:

	float mRotationsPerSec;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/



Rotation operator * ( float ratio, const Rotation& rotation )
{
	Rotation r;
	r.setRPS( rotation.getRPS() * ratio );
	return r;
}

Rotation operator * ( const Rotation& rotation, float ratio )
{
	Rotation r;
	r.setRPS( rotation.getRPS() * ratio );
	return r;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


float Rotation::getRPM() const
{
	return 60 * mRotationsPerSec;
}


float Rotation::getRPS() const
{
	return mRotationsPerSec;
}


float Rotation::getRadiansPS() const
{
	return getRPS() * TwoPi;
}


float Rotation::getRadiansPM() const
{
	return getRadiansPS() * 60;
}


float Rotation::getDegreesPS() const
{
	return getRPS() * 360;
}


float Rotation::getDegreesPM() const
{
	return getDegreesPS() * 60;
}	


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/
/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Rotation::setRPM( float value )
{
	mRotationsPerSec = value / 60;
}


void
Rotation::setRPS( float value )
{
	mRotationsPerSec = value;
}


void
Rotation::setRadiansPS( float value )
{
	setRPS( value / TwoPi );
}


void
Rotation::setRadiansPM( float value )
{
	setRadiansPS( value / 60 );
}


void
Rotation::setDegreesPS( float value )
{
	setRPS( value / 360 );
}


void
Rotation::setDegreesPM( float value )
{
	setDegreesPS( value / 60 );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

} // namespace ACC{

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __ACC_ROTATION_HPP__