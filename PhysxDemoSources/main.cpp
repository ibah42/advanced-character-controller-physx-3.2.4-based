/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/


#include "PhysxDemoSources/PCH.hpp"
#include "PhysxDemoSources/Demo.hpp"


#pragma comment ( lib, "freeglut.lib" )
#pragma comment ( lib, "glew32.lib" )

int main(int argc,char** argv)
{
	try 
	{
		Demo demo( argc, argv );
	}
	catch( const std::exception& e )
	{
		std::cout<<"std::exception based exception occured :: " << e.what();
		return 1;
	}
	catch(...)
	{
		std::cout<<"Undefined exception occured\n";
		return 42;
	}

	return 0;
}