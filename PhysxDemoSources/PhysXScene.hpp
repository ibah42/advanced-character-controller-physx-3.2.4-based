/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __PhysX_SCENE_HPP__
#define __PhysX_SCENE_HPP__

#include "ACCSources/CharacterController/CCManager.hpp"
#include "PhysxDemoSources/PhysXWorld.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace ACC;
using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class PhysxScene
{
public:

	PhysxScene( PhysxWorld& world, PxSceneDesc* desc = 0 );
	~PhysxScene();
	const PxRenderBuffer& getPxRenderBuffer() { return mScene->getRenderBuffer(); }

	void update( float dt );
	void createStairs( const float hight, const float depth, const PxVec3 pos, const int count );

	void createRamp( float angle, float length, float width,const PxVec3 pos );
	void createRandomBoxes( int count,  PxVec3 center, float deviation, float sizeDelta );

	void createKinematicMovementSphere( PxVec3 pos, float radius, PxVec3 move, float maxDist );
	void createKinematicMovement( PxVec3 pos, PxVec3 extents, PxVec3 move, float maxDist );
	void updateKinematics( float dt );
	void createMeshCarrot( PxVec3 pos, PxQuat q, float scale = 1 );
	void createTrigMeshCone( PxVec3 pos, PxQuat q, float scale = 1 );
	void createTrigMeshConeEx( float depth, int count, float scale = 1 );


	void createTrigs( PxVec3 pos, PxQuat q, int size, PxVec3* buff, PxU32* trigs, int trigsCount );
	void createMesh( PxVec3 pos, PxQuat q, int size, PxVec3* buff );
	PxRigidStatic* createHeightField(PxReal* heightmap, PxReal hfScale, PxU32 hfSize);

	void createMeshCarrotEx( float depth, int count, float scale );
public:

	struct KinematicVolume
	{
		PxVec3 extent;
		PxVec3 originPos;
		PxRigidDynamic* actor;
		float maxDist;
		PxVec3 speed;
		bool canSwitch;
	};

	std::vector< KinematicVolume > mKinematics;
	PxMaterial * mDefaultMaterial;
	PhysxWorld& mWorld;
	PxScene* mScene;
	float mTimeStep;

	ACC::CCManager* mManager;

}; // class Scene

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class MemoryOutputStream
	: public PxOutputStream
{
public:
	
	MemoryOutputStream()
		:	mData(NULL)
		,	mSize(0)
		,	mCapacity(0)
	{}
	
	virtual	~MemoryOutputStream()
	{
		if(mData)
			delete[] mData;
	}

	PxU32 write(const void* src, PxU32 size)
	{
		PxU32 expectedSize = mSize + size;
		if(expectedSize > mCapacity)
		{
			mCapacity = expectedSize + 4096;

			PxU8* newData = new PxU8[mCapacity];
			PX_ASSERT(newData!=NULL);

			if(newData)
			{
				memcpy(newData, mData, mSize);
				delete[] mData;
			}
			mData = newData;
		}
		memcpy(mData+mSize, src, size);
		mSize += size;
		return size;
	}


	PxU32		getSize()	const	{	return mSize; }
	PxU8*		getData()	const	{	return mData; }
private:
	PxU8*		mData;
	PxU32		mSize;

	PxU32		mCapacity;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class MemoryInputData: public PxInputData
{
public:
	MemoryInputData(PxU8* data, PxU32 length);

	PxU32		read(void* dest, PxU32 count);
	PxU32		getLength() const;
	void		seek(PxU32 pos);
	PxU32		tell() const;

private:
	PxU32		mSize;
	const PxU8*	mData;
	PxU32		mPos;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __PhysX_SCENE_HPP__
