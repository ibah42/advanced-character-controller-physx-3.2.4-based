/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __PHYSX_World_HPP__
#define __PHYSX_World_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;
using namespace ACC;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class PhysxWorld
	:	public physx::debugger::comm::PvdConnectionHandler
{
public:
	
	PhysxWorld();
	virtual ~PhysxWorld();

	virtual void onPvdSendClassDescriptions( physx::debugger::comm::PvdConnection& connection );
	virtual void onPvdConnected( physx::debugger::comm::PvdConnection& connection );
	virtual void onPvdDisconnected( physx::debugger::comm::PvdConnection& connection );
	
	void togglePvdConnection();

public:

	PxTolerancesScale			& getDefaultToleranceScale()	{ return mDefaultToleranceScale;		}

	PxFoundation				& getFoundation()				{ return	*	mFoundation;			}
	PxProfileZoneManager		& getProfileZoneManager()		{ return	*	mProfileZoneManager;	}
	PxPhysics					& getPhysics()					{ return	*	mPhysics;				}
	PxControllerManager			& getControllerManager()		{ return	*	mControllerManager;		}
	pxtask::CudaContextManager	* getCudaContextManager()		{ return		mCudaContextManager;	}
	PxCooking					& getCooking()					{ return	*	mCooking;				}
	PxDefaultCpuDispatcher		* getCpuDispatcher()			{ return		mCpuDispatcher;			}
	pxtask::GpuDispatcher		* getGpuDispatcher()			{ return		mGpuDispatcher;			}
	PxMaterial					& getDefaultMaterial()			{ return	*	mDefaultMaterial;		}

	
	static PxFilterFlags getSimulationFilterCallback(
			PxFilterObjectAttributes attributes0
		,	PxFilterData filterData0
		,	PxFilterObjectAttributes attributes1
		,	PxFilterData filterData1
		,	PxPairFlags& pairFlags
		,	const void* constantBlock
		,	PxU32 constantBlockSize
	);

	bool isCorrect(){ return mIsCorrect; }
	
protected:	

	void createPvdConnection();
	void release();

private:

	bool mIsCorrect;

	PxTolerancesScale			mDefaultToleranceScale;

	PxFoundation				*mFoundation;
	PxProfileZoneManager		*mProfileZoneManager;
	PxPhysics					*mPhysics;
	PxControllerManager			*mControllerManager;
	pxtask::CudaContextManager	*mCudaContextManager;
	PxCooking					*mCooking;
	PxDefaultCpuDispatcher		*mCpuDispatcher;
	pxtask::GpuDispatcher		*mGpuDispatcher;
	PxMaterial					*mDefaultMaterial;
	PxFilterFlags				*mDegaultFilterFlags;

private:

	PxDefaultErrorCallback		mDefaultErrorCallback;
	PxDefaultAllocator			mDefaultAllocatorCallback;

}; // class World

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __PHYSX_World_HPP__
