/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/


#include "PhysxDemoSources/PCH.hpp"
#include "PhysxDemoSources/Demo.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

static SceneSettings* gSceneSettings = 0;
static Demo* gDemo = 0;
static bool isDemoInitialized = false;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace ACC;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


SceneSettings::SceneSettings()
	:	mFOV( 90.f )
	,	mWinSizeHeight(800)
	,	mWinSizeWidth(900)
	,	mFarClip( 10000 )
	,	mNearClip( 0.0001f )
{
} // SceneSettings::SceneSettings


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Demo::~Demo()
{
	mScene.reset();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


Demo::Demo( int argc, char **argv )
	:	mScene( 0 )
	,	mPrevMousePosY( mSceneSettings.mWinSizeHeight / 2 )
	,	mPrevMousePosX( mSceneSettings.mWinSizeWidth / 2 )
	,	mCamera(
				PxVec3( 20, 20, -20 )
			,	PxVec3( -10, -8, 20 )
			,	PxVec3( 0, 1, 0 )
			,	-80.f
			,	80.f
		)
	,	mTime1(0)
	,	mTime2(0)
	,	mCurrentFreq(60)
	,	mAcceleration( 4 )
{
	gSceneSettings = &mSceneSettings;
	assert( !isDemoInitialized );
	
	gDemo = this;
	isDemoInitialized = true;

	if( ! mWorld.isCorrect() )
		CRITICAL_MESSAGE("world build crashed");
	
	mScene.reset( new PhysxScene( mWorld ) );
	
	// init GLUT and create Window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA |  GLUT_STENCIL);
	glutInitWindowPosition(200,200);
	glutInitWindowSize( gSceneSettings->mWinSizeWidth, gSceneSettings->mWinSizeHeight );
	glutCreateWindow( "Advanced Character Controller Demo (PhysX 3.2.4 KCC based)" ); 


 	glutDisplayFunc( &Demo::display );
 	glutSpecialFunc( &Demo::specialKeys );
 	glutKeyboardFunc( &Demo::regularKeys );
 	glutReshapeFunc( &Demo::reshapeWindow );
 	glutMouseFunc( &Demo::mouseClickEvent );
 	glutPassiveMotionFunc( &Demo::mouseDragEvent );
	glutMotionFunc( &Demo::mouseDragEvent );

	
	glutReshapeWindow( gSceneSettings->mWinSizeWidth, gSceneSettings->mWinSizeHeight );

	initFrame();

	mUserCC = *( gDemo->mScene->mManager->getCharacters().begin() );

	try
	{
		glutMainLoop();
	}
	catch( std::exception & e )
	{
		std::cout << "Std::Exception occured :: " << e.what() << std::endl;
	}
	catch( ... )
	{
		std::cout << "UNKNOWN Exception occured !" << std::endl;
	}
	
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Demo::mouseClickEvent ( int button, int state, int x, int y )
{
	glutPostRedisplay();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Demo::reshapeWindow ( int _w,int _h )
{
	gSceneSettings->mWinSizeWidth = _w;
	gSceneSettings->mWinSizeHeight = _h;
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(
			gSceneSettings->mFOV
		,	gSceneSettings->mWinSizeWidth / (float)gSceneSettings->mWinSizeHeight
		,	gSceneSettings->mNearClip
		,	gSceneSettings->mFarClip
	);

	glViewport( 0, 0, gSceneSettings->mWinSizeWidth, gSceneSettings->mWinSizeHeight );
	glutPostRedisplay();
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Demo::display() 
{
	float dt = gDemo->mTimer.getElapsedTime();
	if ( dt > 0 )
	{
		gDemo->mTime1 += dt;

		ACC::clamp<float>( 0, 0.5f, dt );
		gDemo->mTimer.reset();

		glClear( GL_COLOR_BUFFER_BIT |  GL_DEPTH_BUFFER_BIT |  GL_STENCIL_BUFFER_BIT );
		glShadeModel(GL_SMOOTH); 
		glEnable(GL_DEPTH_TEST);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity ();

		gDemo->mCamera.defaultPrefsLookAt();
		drawHomeGrid( 200 );	
	
		gDemo->analyzeKeyboard();
		gDemo->mScene->update( dt );
	
		gDemo->mPhysxRenderer.begin();
		gDemo->mPhysxRenderer.render( gDemo->mScene->getPxRenderBuffer() );
		//gDemo->m_physxRenderer.update( gDemo->m_world.getControllerManager().getRenderBuffer() );
		gDemo->mCamera.pos() = ACC::vectorXFlip( gDemo->mUserCC->getInterpolatedPosition() );
	
		const ACC::CCManager::CCSet& chars = gDemo->mScene->mManager->getCharacters();
		ACC::CCManager::CCSet::iterator begin = chars.cbegin();
		ACC::CCManager::CCSet::iterator end = chars.cend();
		while( begin != end )
		{
			ACC::RefOptional< ACC::ImpactVector > contact = (*begin)->getFootingImpactData();
			if ( contact )
			{
				PxU32 color =
						50  * 0x100* 0x100 * 0x100
					+	150 * 0x100* 0x100;
					+	100 * 0x100			
					+	0
				;

				gDemo->mPhysxRenderer.renderLine( contact->point, contact->point + contact->normal*2, color );
			}

			ACC::RefOptional< PxVec3 > dir = (*begin)->getBestFootingFaceDir();
			if ( dir && contact )
			{
				PxU32 color =
						50  * 0x100* 0x100 * 0x100
					+	250 * 0x100* 0x100;
					+	0 * 0x100			
					+	0
				;

				gDemo->mPhysxRenderer.renderLine( contact->point, contact->point + *dir*2, color );
			}
			++begin;
		}

		gDemo->mPhysxRenderer.end();
		gDemo->showFPS( dt );
	}

	if ( gDemo->mPrevMousePosX != gSceneSettings->mWinSizeWidth/2 || gDemo->mPrevMousePosY != gSceneSettings->mWinSizeHeight/2 )
	{
		gDemo->mPrevMousePosX = gSceneSettings->mWinSizeWidth/2;
		gDemo->mPrevMousePosY = gSceneSettings->mWinSizeHeight/2;
		glutWarpPointer( gDemo->mPrevMousePosX, gDemo->mPrevMousePosY );
	}

	glutSetCursor(GLUT_CURSOR_NONE);
	glutPostRedisplay();
	glutSwapBuffers();

} // Callbacks::display


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Demo::regularKeys ( unsigned char key, int x, int y ) 
{
	switch(key) 
	{

	case 27:  // Escape to quit
		exit(0);
		break;

	default:
		break;
	}

} // Demo::regularKeys


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Demo::specialKeys(int key, int x, int y)
{
	if(key == GLUT_KEY_UP)
	{

	}
	if(key == GLUT_KEY_DOWN)
	{

	}
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Demo::mouseDragEvent( int x, int y )
{
	gDemo->correctCameraRotation( x, y );
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Demo::initFrame ()
{
	glShadeModel(GL_FLAT);                    // shading method: GL_SMOOTH or GL_FLAT
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);      // 4-byte pixel alignment

	// enable /disable features
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	//glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	//glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
	//glEnable(GL_DEPTH_TEST);
	//glEnable(GL_LIGHTING);
	//glEnable(GL_TEXTURE_2D);
	//glEnable(GL_CULL_FACE);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable( GL_BLEND );

	// track material ambient and diffuse from surface color, call it before glEnable(GL_COLOR_MATERIAL)
	//glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	//glEnable(GL_COLOR_MATERIAL);

	glClearColor( 0.5f, 0.5f, 0.5f, 0.5f );      // background color
	glClearStencil(0);                          // clear stencil buffer

} // Demo::InitFrame


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Demo::drawHomeGrid ( GLfloat _length )
{
	const float size = 200.f;
	glDisable ( GL_LIGHTING ) ;
	glLineWidth(1.f);

	glColor3f(0.3f,0.3f,0.3f);
	glBegin(GL_LINES);
	for ( GLfloat i=0; i<=size; ++i )
	{
		if ( ACC::cmpEqual( i, size / 2.f, 0.01f ) )
		{
			glColor3f(0.f,0.f,0.f); 
			glVertex3f( 0.f, 0.f, ACC::linearInterpolation(-_length,_length,i / size));
			glVertex3f( -_length, 0.f, ACC::linearInterpolation(-_length,_length,i / size));
			glColor3f(0.3f,0.3f,0.3f); 
		}
		else
		{
			glVertex3f( _length, 0.f, ACC::linearInterpolation(-_length,_length,i / size));
			glVertex3f( -_length, 0.f, ACC::linearInterpolation(-_length,_length,i / size));
		}
	}
	for ( GLfloat i=0; i<=size; ++ i)
	{
		if ( ACC::cmpEqual( i, size / 2.f, 0.01f ) )
		{
			glColor3f(0.f,0.f,0.f); 
			glVertex3f (ACC::linearInterpolation(-_length,_length,i / size), 0.f, 0.f);
			glVertex3f( ACC::linearInterpolation(-_length,_length,i / size), 0.f,-_length);			
			glColor3f(0.3f,0.3f,0.3f);
		}
		else
		{
			glVertex3f( ACC::linearInterpolation(-_length,_length,i / size), 0.f,_length);
			glVertex3f( ACC::linearInterpolation(-_length,_length,i / size), 0.f,-_length);	
		}
	}

	glEnd();

	// draw axes
	glLineWidth(2.f);
	glBegin(GL_LINES);
	glColor3f(1.f,0.f,0.f);
	glVertex3f(0.f,0.f,0.f);
	glVertex3f(_length,0.f,0.f);

	glColor3f(0.f,1.f,0.f);
	glVertex3f(0.f,0.f,0.f);
	glVertex3f(0.f,_length,0.f);

	glColor3f(0.f,0.f,1.f);
	glVertex3f(0.f,0.f,0.f);
	glVertex3f(0.f,0.f,_length);

	glEnd();
	glEnable(GL_LIGHTING);
	glutPostRedisplay ();	

} // DisplayPrimitives::DrawHomeGrid


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/



bool
Demo::isKeyPressed( unsigned char* array, unsigned char key )
{
	return ( array[ key ] & 0x80 ) > 0;
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Demo::analyzeKeyboard()
{
#ifdef PX_WINDOWS

	unsigned char kb[256];
	if( ! GetKeyboardState( (BYTE*)kb ) )
	{
		exit(0x42);
		return;
	}

	ACC::AbstractCCInput& input = mScene->mManager->getBroadcaster();
	
	input.resetInput();

	if ( isKeyPressed( kb, VK_W ) )
		input.moveForwardOn();
	
	if ( isKeyPressed( kb,VK_S ) )
		input.moveBackwardOn();

	if ( isKeyPressed( kb,VK_A ) )
		input.strafeLeftOn();

	if ( isKeyPressed( kb,VK_D  ) )
		input.strafeRightOn();

	if ( isKeyPressed( kb, VK_SPACE  ) )
		input.jumpOn();

	if ( isKeyPressed( kb, VK_LCONTROL ) )
		input.duckDown();

	if ( isKeyPressed( kb, VK_LSHIFT ) )
		input.run();

#endif // PX_WINDOWS
}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void 
Demo::correctCameraRotation( int x, int y )
{
	float k = gSceneSettings->mFOV * mAcceleration;
	float xDeg = float( x - mPrevMousePosX ) / float( gSceneSettings->mWinSizeWidth ) * k;
	float yDeg = float( y - mPrevMousePosY ) / float( gSceneSettings->mWinSizeHeight ) * k;
		
	mPrevMousePosX = x;
	mPrevMousePosY = y;

	mUserCC->setDirection( mCamera.rotate( xDeg, yDeg ) );
}



/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Demo::setOrthographicProjection()
{
	// switch to projection mode
	glMatrixMode(GL_PROJECTION);

	// save previous matrix which contains the
	//settings for the perspective projection
	glPushMatrix();

	// reset matrix
	glLoadIdentity();

	// set a 2D orthographic projection
	gluOrtho2D(0, gSceneSettings->mWinSizeWidth, gSceneSettings->mWinSizeHeight, 0 );

	// switch back to modelview mode
	glMatrixMode(GL_MODELVIEW);

}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Demo::setPerspectiveProjection()
{
	glMatrixMode(GL_PROJECTION);
	// restore previous projection matrix
	glPopMatrix();

	// get back to modelview mode
	glMatrixMode(GL_MODELVIEW);

}


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


void
Demo::showFPS ( float _dt )
{	
	if ( mTime2 < mTime1 - 0.2f )
	{
		mTime2 = mTime1;
		mCurrentFreq = 1 / _dt;
	}	

	glDisable ( GL_LIGHTING );
	glColor3f ( 1.f,1.f,1.f );
	
	// set orto
	setOrthographicProjection ();

	// calc and show
	std::string strFps( " FPS" );
	ACC::clamp( 0.f, 10000.f, mCurrentFreq );
	char sbuff[50];
	sprintf_s ( sbuff, "%4.1f", mCurrentFreq );
	glPushMatrix();
	glLoadIdentity();
	glRasterPos3f( gSceneSettings->mWinSizeWidth / 2 - 30.f , gSceneSettings->mWinSizeHeight - 10.0f, 0.0f );

	for ( unsigned int i=0;i<4;i++ )
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15 , sbuff[i]);
	
	for ( unsigned int i=0;i<strFps.size(); ++ i )
		glutBitmapCharacter(GLUT_BITMAP_9_BY_15, strFps[i] );

	glPopMatrix();

	// restore perspective
	setPerspectiveProjection ();
	glEnable ( GL_LIGHTING );
	glutPostRedisplay ();

} // Callbacks::ShowFPS


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/