/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __PhysX_DEBUG_GL_RENDER_HPP__
#define __PhysX_DEBUG_GL_RENDER_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

using namespace physx;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class PhysxDebugGLRender
{
public:
	
	void begin();
	void end();

	void render(const PxRenderBuffer& _data);
	void renderLine( const PxVec3& p1, const PxVec3& p2, PxU32 color = 4242424242 );

private:
		
	void renderLines( const PxRenderBuffer& _data  );
	void renderTriangles( const PxRenderBuffer& _data );
	void renderPoints( const PxRenderBuffer& _data );

	void setColor( PxU32 color );

	union transport
	{
		PxU32 source;
		unsigned char color[4];
	};

	inline float r( PxU32 _c )
	{
		transport t;
		t.source = _c;
		unsigned char color = t.color[0];
		return t.color[0] / 255.f;
	}

	inline float g( PxU32 _c )
	{
		transport t;
		t.source = _c;
		unsigned char color = t.color[1];
		return t.color[1] / 255.f;
	}

	inline float b( PxU32 _c )
	{
		transport t;
		t.source = _c;
		unsigned char color = t.color[2];
		return t.color[2] / 255.f;
	}

}; // class PhysxDebugGLRender


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __PhysX_DEBUG_GL_RENDER_HPP__