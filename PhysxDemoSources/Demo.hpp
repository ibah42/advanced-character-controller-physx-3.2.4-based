/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/


#ifndef __DEMO_HPP__
#define __DEMO_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#include "PhysxDemoSources/PhysxDebugGLRender.hpp"
#include "PhysxDemoSources/Camera.hpp"
#include "PhysxDemoSources/PhysXWorld.hpp"
#include "PhysxDemoSources/PhysXScene.hpp"

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#define VK_A 0x41 
#define VK_B 0x42
#define VK_C 0x43
#define VK_D 0x44
#define VK_E 0x45
#define VK_F 0x46 
#define VK_G 0x47
#define VK_H 0x48
#define VK_I 0x49
#define VK_J 0x4A
#define VK_K 0x4B
#define VK_L 0x4C 
#define VK_M 0x4D
#define VK_N 0x4E
#define VK_O 0x4F
#define VK_P 0x50
#define VK_Q 0x51
#define VK_R 0x52
#define VK_S 0x53
#define VK_T 0x54 
#define VK_U 0x55
#define VK_V 0x56
#define VK_W 0x57
#define VK_X 0x58
#define VK_Y 0x59
#define VK_Z 0x5A

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Timer
{
public:

	Timer ()
	{
		reset();
	}

	inline float getElapsedTime () 
	{   
		double tmFinal = clockDouble() - m_tmInit;
		m_tmInit = clockDouble();
		return float( tmFinal / 1000.f ); 
	}

	inline void reset ()
	{
		m_tmInit = clock();
	}

private:

	inline static float clock()
	{
		return float( ::clock() );
	}

	inline static double clockDouble()
	{
		return ::clock();
	}

private:

	double m_tmInit;
};


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


struct SceneSettings
{
public:

	SceneSettings();

public:

	float mFOV;
	float mNearClip;
	float mFarClip;

	int mWinSizeHeight;
	int mWinSizeWidth;

}; // struct SceneSettings


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

extern SceneSettings* gSceneSettings;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/



class Demo
{

public:

	Demo( int argc, char **argv );
	~Demo();

	static void initFrame();
	static void mouseDragEvent( int x, int y );	
	static void reshapeWindow( int w, int h );
	static void display();
	static void mouseClickEvent( int button, int state, int x, int y );
	static void regularKeys( unsigned char key,int x,int y );
	static void specialKeys( int key, int x, int y );

	static void drawHomeGrid ( GLfloat _length );
	
	void showFPS ( float dt );
	void setOrthographicProjection();
	void setPerspectiveProjection();
	void analyzeKeyboard();

	static bool isKeyPressed( unsigned char* array, unsigned char key );

	void correctCameraRotation( int x, int y );
public:

	SceneSettings mSceneSettings;

public:

	float mAcceleration;
	float mCurrentFreq;
	float mTime1;
	float mTime2;
	Camera mCamera;
	int mPrevMousePosX;
	int mPrevMousePosY;
	ACC::AbstractCC* mUserCC;
	PhysxDebugGLRender mPhysxRenderer;
	Timer mTimer;
	PhysxWorld mWorld;
	std::auto_ptr< PhysxScene > mScene;

}; // class Demo


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

extern Demo* gDemo;

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


#endif //__DEMO_HPP__



