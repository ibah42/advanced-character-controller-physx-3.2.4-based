/*
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	Advanced Character Controller. Character controller simulation library based on PhysX 3.2.4 Kinematic Character Controller.
	Copyright (C) 2013-2014,  Ivan Voinyi

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.

	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*

	For support and feedback, please contact:								ibah.acc@gmail.com
	For license related questions, commercial proposals, please contact:	ibah.acc@gmail.com
	
	*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*
*/

#ifndef __CAMERA_HPP__
#define __CAMERA_HPP__

/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/


class Camera
{

public:

	Camera( PxVec3& pos, PxVec3& dir, PxVec3 up, float lowDegCone, float highDegCone );
	const PxVec3& rotate( float _xDeg, float _yDeg );

	inline			PxVec3& up()				{ return mUp;  }
	inline	const	PxVec3& dir() const			{ return mDir; }
	inline			PxVec3& pos()				{ return mPos; }
	
	void defaultPrefsLookAt();

	float currentPitchDegree() const;
private:

	float mLowDegCone;
	float mHighDegCone;
	PxVec3 mUp;
	PxVec3 mDir;
	PxVec3 mPos;

}; // class Camera


/*-----~-----~-----~-----~-----~-----~-----~-----~-----~-----~-----*/

#endif // __CAMERA_HPP__